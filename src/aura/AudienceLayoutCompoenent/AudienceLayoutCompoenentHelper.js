({
	drawAudienceVenueLayout : function(component) {
        debugger;
        
        var globalID = component.getGlobalId();
        console.log(component.getGlobalId());
        var numberOfSeats = component.get("v.wrapperObj.NumberOfSeats");
 		
        /* Edit Layout
           Added by: Ankush
           Date    : 11/07/2017 */
        
       var obj =  component.get("v.wrapperObj");
       var stion = component.get("v.section");
        console.log("obj+++++++" +obj);
       var parentdiv = document.getElementById(globalID); 
       parentdiv.addEventListener("click",$A.getCallback(function myFunction(component,event){
       var sObjectEvent = $A.get("e.c:EditLayoutEvent");
       		sObjectEvent.setParams({
            	"message" : obj,
                "sections":	stion
            });
        sObjectEvent.fire();
        }));
        
        
       /*Edit Layout End*/ 
        
        
        // Audience
        var totalSeats = component.get("v.wrapperObj.NumberOfSeats") 
        var capacity = Math.round(totalSeats/component.get("v.section"));  ;//50;
        
        var seatsPerRow = component.get("v.wrapperObj.SeatsPerRow");//10;
        var numberOfRows = Math.round(capacity/seatsPerRow);
       
        console.log("capacity" + capacity);
        console.log("seatsPerRow" + seatsPerRow);
        console.log("numberOfRows" + numberOfRows);
        console.log("remainingSeatsCount" + remainingSeatsCount);
        
        var sections = component.get("v.section");
        var remainingSeatsCount = (totalSeats % component.get("v.section"));
        var columns = 1;
		var rowList = [];
         //var sequence = 1;
        //var row = 1;
        var totalSeatsOfLayout = 1;
        for(var sec=0;sec<sections;sec++){
        	
            var remainingSeatsOfSection = capacity % numberOfRows ;
            if(remainingSeatsOfSection < seatsPerRow){
                 numberOfRows = numberOfRows+1;
            }
            var section = document.getElementById(globalID + '_section');
            var rowTemplate = document.getElementById(globalID + '_seat-template');
            var sectionLi = document.createElement('li');
            sectionLi.id = sec + '_sectionLi';
            sectionLi.className = 'sectionLi';
			rowTemplate.appendChild(sectionLi);            
            var sectionUl =  document.createElement('ul');
            var sectionRowTemplate = document.getElementById(sec + '_sectionLi');
            sectionUl.id = sec + '_sectionUl';
            sectionRowTemplate.appendChild(sectionUl);
            
            
            var seatsPerRowVar = 0; 
            var seatCountPerRow = 1;
            var seatsPerSectionVar = 0;
          	for(var cap=0;cap<numberOfRows;cap++){
			
                var newSection = document.getElementById(sec + '_sectionUl');
                var rowLi = document.createElement('li');
                rowLi.id = globalID + '_rowLi';
                var rowUl = document.createElement('ul');
                rowUl.id = globalID + '_rowUl';
                rowUl.style.margin = '3px';
                newSection.appendChild(rowLi);

                for(var row=0;row<seatsPerRow;row++){
                    if(seatsPerSectionVar < capacity && totalSeatsOfLayout <= numberOfSeats){
                     	
                        var seatLi = document.createElement('li');
                        seatLi.className = 'chair';
						var label = document.createElement('label')
                        label.htmlFor = "seatLabel"+seatsPerSectionVar;
                        label.className = "seatLabel";
                        label.innerHTML = 'S'+ (totalSeatsOfLayout);
                        seatLi.appendChild(label);
                        rowUl.appendChild(seatLi);
                    	seatsPerRowVar++;
                    	seatsPerSectionVar++;
                        totalSeatsOfLayout++;
                    }
                }
                rowLi.appendChild(rowUl);
                var seatTemplate = $("ul[id='"+sec+"_sectionUl"+"']");
                seatTemplate.append(rowLi);
           		seatCountPerRow++;
            }
        }
    }
  
})