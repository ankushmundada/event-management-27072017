public class DesignLayoutTypeWrapper {
		
    	@AuraEnabled
        public String defaultLayoutType  { get; set; }
         @AuraEnabled
    	Public String guid { get; set; }
         @AuraEnabled
    	public Integer numberOfTables { get; set; }
         @AuraEnabled
    	public DesignLayoutWrapper.Table table { get; set; }
         @AuraEnabled
        public Integer layoutRowCount { get; set; }

    	// total seat count
    	@AuraEnabled
    	public Integer totalSeats  { get; set; }
    	// Seats per row for audience
    	@AuraEnabled
        public Integer SeatsPerRow { get; set; }
    	// Starting sequence for a Seat, can be alphabetical
    	@AuraEnabled
        public String SeatSequenceStart  { get; set; }
    	// Position for the layout
    	@AuraEnabled
        Public DesignLayoutWrapper.Position position  { get; set; }

    	// This map will be used to map seats for a table based on registration and layout rendering purpose
    	@AuraEnabled
        public Map<String, Event_Registration__c> seatToObjectMap;
}